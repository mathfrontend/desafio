// Atualiza todos os cards
function updateCards() {
    if (!filtroCamisetas) {
        updateCamiseta()
    }
    
    if (!filtroCalcas) {
        updateCalcas()  
    }
    
    if (!filtroCalcados) {
        updateCalcados()
        
    }
}
updateCards()


// Eventos Dos botões do Menu e Categorias
// ------------------------------------------

// Filtro Categorias
// ----------------

// VAR CATEGORIAS
var filtroCamisetas = false
var filtroCalcados = false
var filtroCalcas = false

// CAMISETAS

function atualizarCamisetas() {
    if (filtroCamisetas) {
        limparCategoria()
        filtroCamisetas = false
    } else {
        $('.Camiseta').show();
        $(".card").filter(".card:visible").not('.Camiseta, #filtragem').hide();
        $("#sessaoatual, #paginaatual").empty();
        $('#sessaoatual, #paginaatual').append(productnames.items[0].name)
        $('#categoriacamisetas').removeClass("text-muted");
        $('#categoriacalcas').addClass("text-muted");
        $('#categoriacalcados').addClass("text-muted");
        $(".filtrocores a, .filtrotipo a").show();
        limparCategoria()
        filtroCamisetas = true
    }
}
$("#botaocamisetas, #categoriacamisetas").click(function(){
    atualizarCamisetas()
});

// CALÇAS

function atualizarCalcas () {
    if (filtroCalcas) {
        limparCategoria()
        filtroCalcas = false
    } else {
        $('.Calça').show();
        $(".card").filter(".card:visible").not('.Calça, #filtragem').hide();
        $("#sessaoatual, #paginaatual").empty();
        $('#sessaoatual, #paginaatual').append(productnames.items[1].name)
        $('#categoriacalcas').removeClass("text-muted");
        $('#categoriacamisetas').addClass("text-muted");
        $('#categoriacalcados').addClass("text-muted");
        $(".filtrocores a, .filtrotipo a").show();
        limparCategoria()
        filtroCalcas = true
    }
}
$('#botaocalcas, #categoriacalcas').click(function(){
    atualizarCalcas()
})

// CALÇADOS

function atualizarCalcados() {
    if (filtroCalcados) {
        limparCategoria()
        filtroCalcados = false
    } else {
        $('.Tênis').show();
        $(".card").filter(".card:visible").not('.Tênis, #filtragem').hide();
        $("#sessaoatual, #paginaatual").empty();  
        $('#sessaoatual, #paginaatual').append(productnames.items[2].name)
        $('#categoriacalcados').removeClass("text-muted");      
        $('#categoriacalcas').addClass("text-muted");
        $('#categoriacamisetas').addClass("text-muted");    
        $(".filtrocores a, .filtrotipo a").show();
        limparCategoria()
        filtroCalcados = true
    }
}

$('#botaocalcados, #categoriacalcados').click(function(){
    atualizarCalcados()
})

// --------------------------------------------------
// Eventos dos botões dos Filters Cores

// VAR CORES
var filtropreto = false 
var filtrolaranja = false 
var filtroamarelo = false 
var filtrorosa = false 
var filtrocinza = false 
var filtroazul = false 
var filtrobege = false 

// EVENTOS    
// PRETO
$("#filtropreto").click(function() { 
    if (filtropreto) {
        limparCores()
        filtropreto = false
    } else {
        $(".card").filter(".card:visible").not('.Preta, #filtragem').hide();
        $(".filtrocores a").not('#filtropreto').hide();
        filtropreto = true
    }
}); 
// LARANJA
$("#filtrolaranja").click(function() { 
    if (filtrolaranja) {
        limparCores()
        filtrolaranja = false
    } else {
        $(".card").filter(".card:visible").not('.Laranja, #filtragem').hide(); 
        $(".filtrocores a").not('#filtrolaranja').hide();
        filtrolaranja = true
    }        
}); 
// AMARELO
$("#filtroamarelo").click(function() { 
    if (filtroamarelo) {
        limparCores()
        filtroamarelo = false
    } else {
        $(".card").filter(".card:visible").not('.Amarela, #filtragem').hide();
        $(".filtrocores a").not('#filtroamarelo').hide();
        filtroamarelo = true
    }
}); 
// ROSA
$("#filtrorosa").click(function() { 
    if (filtrorosa) {
        limparCores()
        filtrorosa = false
    } else {
        $(".card").filter(".card:visible").not('.Rosa, #filtragem').hide(); 
        $(".filtrocores a").not('#filtrorosa').hide();
        filtrorosa = true
    }
}); 
// CINZA
$("#filtrocinza").click(function() { 
    if (filtrocinza) {
        limparCores()
        filtrocinza = false
    } else {
        $(".card").filter(".card:visible").not('.Cinza, #filtragem').hide();
        $(".filtrocores a").not('#filtrocinza').hide(); 
        filtrocinza = true
    }
    
}); 
// AZUL
$("#filtroazul").click(function() { 
    if (filtroazul) {
        limparCores()
        filtroazul = false
    } else {
        $(".card").filter(".card:visible").not('.Azul, #filtragem').hide();
        $(".filtrocores a").not('#filtroazul').hide(); 
        filtroazul = true
    } 
}); 
// BEGE
$("#filtrobege").click(function() { 
    if (filtrobege) {
        limparCores()
        filtrobege = false
    } else {
        $(".card").filter(".card:visible").not('.Bege, #filtragem').hide(); 
        $(".filtrocores a").not('#filtrobege').hide();
        filtrobege = true
    }
    
});
// ---------------------------------------------
// FILTRO TIPO 

// VAR TIPOS
var filtrocorrida = false
var filtrocaminhada = false
var filtrosocial = false

// CORRIDA
$("#filtrocorrida").click(function() { 
    if (filtrocorrida) {
        limparTipos()
        filtrocorrida = false
    } else {
        $(".card").filter(".card:visible").not('.Corrida, #filtragem').hide();
        $(".filtrotipo a").not('#filtrocorrida').hide();
        filtrocorrida = true
    }
}); 
// CAMINHADA
$("#filtrocaminhada").click(function() { 
    if (filtrocaminhada) {
        limparTipos()
        filtrocaminhada = false
    } else {
        $(".card").filter(".card:visible").not('.Caminhada, #filtragem').hide();
        $(".filtrotipo a").not('#filtrocaminhada').hide();
        filtrocaminhada = true
    }   
}); 
// SOCIAL
$("#filtrosocial").click(function() { 
    if (filtrosocial) {
        limparTipos()
        filtrosocial = false
    } else {
        $(".card").filter(".card:visible").not('.Social, #filtragem').hide();
        $(".filtrotipo a").not('#filtrosocial').hide();
        filtrosocial = true
    }
    
}); 

// -----------------------------------------

// Botão limpar todos os filtros
function limparFiltros() {
    filtroCamisetas = true
    filtroCalcas = true
    filtroCalcados = true
    $(".card").filter(".card:hidden").show(); 
    $("#sessaoatual, #paginaatual").empty();  
    $('#sessaoatual, #paginaatual').append('Roupas');   
    $('#categoriacalcas, #categoriacamisetas, #categoriacalcados').removeClass("text-muted");
    $(".filtrocores a, .filtrotipo a").show();
}
$('#paginainicial, .limparcategoria').bind('click',(function(){
    limparFiltros()
}))

function limparCategoria() {
    filtroCamisetas = false
    filtroCalcas = false
    filtroCalcados = false
}

function limparCores() {
    filtropreto = false 
    filtrolaranja = false 
    filtroamarelo = false 
    filtrorosa = false 
    filtrocinza = false 
    filtroazul = false 
    filtrobege = false 
}

// ---------------------------

// Input Buscar
function functionBuscar() {
    var input, filter, cards, cardContainer, h5, title, i;
    input = document.getElementById("myFilter");
    filter = input.value.toUpperCase();
    cardContainer = document.getElementById("ofertas");
    cards = cardContainer.getElementsByClassName("card");
    for (i = 0; i < cards.length; i++) {
        title = cards[i].querySelector(".card-body p.card-title");
        if (title.innerText.toUpperCase().indexOf(filter) > -1) {
            cards[i].style.display = "";
        } else {
            cards[i].style.display = "none";
        }
    }
}

// -------------------------------------------

// Botões Grid
$('.botaogrid').click(function () {
    var $this = $(this);
    
    $('.botaogrid').removeClass('active');
    $this.addClass('active');
    
    $('.ofertas .card').removeClass('grid-maior grid-menor').addClass($this.data('class'));
});

