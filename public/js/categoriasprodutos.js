// API LISTA DOS PRODUTOS (LIST)
function updateProductsList(){
  
  $.ajax({
    method: "GET",
    url: 'http://localhost:8888/api/V1/categories/list',
    error: function(response){
      console.log(response);
    },
    success: function(response){
      // MENU
      $('#botaocamisetas').append(response.items[0].name) 
      $('#botaocalcas').append(response.items[1].name) 
      $('#botaocalcados').append(response.items[2].name)
      // Categorias
      $('#categoriacamisetas').append(response.items[0].name) 
      $('#categoriacalcas').append(response.items[1].name) 
      $('#categoriacalcados').append(response.items[2].name)   
    }
  });
}

updateProductsList()
 
// Gambiarra para consertar o nome da section :(
const productnames = {
  "items": [
    {"name": "Camisetas",},
    {"name": "Calças",},
    {"name": "Calçados",}
  ]
}
