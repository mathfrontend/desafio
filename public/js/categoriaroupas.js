// API CAMISETAS
function updateCamiseta(){
  
  $.ajax({
    method: "GET",
    url: 'http://localhost:8888/api/V1/categories/1',
    error: function(response){
      console.log(response);
    },
    success: function(response){
      // console.log(response)
      $( response.items ).each(function( index, item ) {
        // console.log(item)
        $('#ofertas').append("<div style='text-align: -webkit-center;' class='card m-1 grid-maior "+item.name+" "+item.filter[0].color+"' id='"+item.id+"'><img src='"+item.image+"' class='card-img-top img-thumbnail' alt='"+item.path+"'><div class='card-body text-center px-0 d-flex flex-column'><p class='card-title mb-auto'>"+item.name+"</p><p class='card-text vermelho'><span id='preço-especial'></span><span class='preco' id='preco'>R$ "+item.price+"</span></p><a href='#' class='btn btn-azul font-weight-bold'>COMPRAR</a></div></div>");    
      });
    }
  });
}


// API CALÇAS 
// ----------------------------
function updateCalcas(){
  
  $.ajax({
    method: "GET",
    url: 'http://localhost:8888/api/V1/categories/2',
    error: function(response){
      console.log(response);
    },
    success: function(response){
      // console.log(response)
      $( response.items ).each(function( index, item ) {
        // console.log(item)
        $('#ofertas').append("<div style='text-align: -webkit-center;' class='card m-1  grid-maior "+item.name+" "+item.filter[0].gender+"' id='"+item.id+"'><img src='"+item.image+"' class='card-img-top img-thumbnail' alt='"+item.path+"'><div class='card-body text-center px-0 d-flex flex-column'><p class='card-title mb-auto'>"+item.name+"</p><p class='card-text vermelho'><span id='preço-especial'></span><span class='preco' id='preco'>R$ "+item.price+"</span></p><a href='#' class='btn btn-azul font-weight-bold'>COMPRAR</a></div></div>");    
      });
    }
  });
}


// API CALÇADOS
// ---------------------------
function updateCalcados(){
  
  $.ajax({
    method: "GET",
    url: 'http://localhost:8888/api/V1/categories/3',
    error: function(response){
      console.log(response);
    },
    success: function(response){
      // console.log(response)
      $( response.items ).each(function( index, item ) {
        // console.log(item)
        $('#ofertas').append("<div style='text-align: -webkit-center;' class='card m-1 grid-maior "+item.name+" "+item.filter[0].color+"' id='"+item.id+"'><img src='"+item.image+"' class='card-img-top img-thumbnail' alt='"+item.path+"'><div class='card-body text-center px-0 d-flex flex-column'><p class='card-title mb-auto'>"+item.name+"</p><p class='card-text vermelho'><span id='preço-especial'></span><span class='preco' id='preco'>R$ "+item.price+"</span></p><a href='#' class='btn btn-azul font-weight-bold'>COMPRAR</a></div></div>");    
      });
    }
  });
}

