# LEIA-ME
Bom o teste, Diversas maneiras de fazer, pensar, gostei!
Bom, tive apenas o fim de semana para fazer, e após ler na documentação do teste escrito => "queremos ver como você se sai em situações reais",  pensei comigo: não é para ser um sistema planejado, com uma lógica aprimorada, eles realmente querem ver na hora do desespero um "faz funcionar", então tomei a decisão de fazer algo bem primitivo, apenas com eventos Jquery nos botões, apenas funcional e nada mais!

# Técnologias Utilizadas
Utilizei a framework Bootstrap para o layout, pois é uma FW que eu domino, responsivo, com algumas adaptações, porém responsivo!
Utilizei também a biblioteca Jquery, da qual reduz 80% de qualquer código Javascript, gosto de Jquery por ser simples, rápido e prático. Fiz o Ajax no Jquery e todos os eventos dos botões também!
E também, mesmo que seja quase invisível, utilizei fontawesome para ícone!
Para rodar, "npm start" dentro da pasta do projeto e pronto.
Obrigado, Aguardo Retorno!
# Espero que esteja dentro de suas expectativas!